﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BSA21_Lecture4.DAL.Migrations
{
    public partial class FixTaskTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Projects_UserId",
                table: "Tasks");

            migrationBuilder.DropIndex(
                name: "IX_Tasks_UserId",
                table: "Tasks");

            migrationBuilder.RenameColumn(
                name: "PerformerId",
                table: "Tasks",
                newName: "ProjectId");

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 24, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 52, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 67, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 76, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 86, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 82, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 110, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 88, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 69, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 27, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 60, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 75, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 77, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 83, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 52, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 90, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 106, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 98, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 84, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 111, 5 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 116, 5 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 92, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 54, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 44, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 66, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 62, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 123, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 50, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 47, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 84, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 119, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 79, 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 35, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 40, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 123, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 63, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 92, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 94, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 63, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 101, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 90, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 78, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 47, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 99, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 120, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 81, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 62, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 66, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 103, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 81, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 64, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 118, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 23, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 73, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 73, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 33, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 34, 11 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 97, 11 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 64, 11 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 100, 11 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 81, 11 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 111, 11 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 82, 11 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 88, 11 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 75, 11 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 104, 12 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 97, 12 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 52, 12 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 103, 12 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 110, 13 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 65, 13 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 36, 13 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 101, 13 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 85, 13 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 88, 13 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 115, 13 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 78, 13 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 75, 14 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 83, 14 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 58, 14 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 76, 14 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 123, 15 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 120, 15 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 26, 15 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 75, 16 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 97, 16 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 52, 18 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 115, 18 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 63, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 42, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 37, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 114, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 117, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 63, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 118, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 60, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 21, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 123, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 82, 20 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 92, 20 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 97, 20 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 82, 20 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 109, 20 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 67, 20 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 34, 20 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 57, 20 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 87, 20 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 57, 21 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 26, 21 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 111, 21 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 80, 21 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 108, 21 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 62, 23 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 55, 24 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 28, 24 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 27, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 53, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 109, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 28, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 80, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 55, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 85, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 92, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 23, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 28, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 40, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 21, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 102, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 108, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 28, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 131,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 98, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 132,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 46, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 133,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 77, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 134,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 105, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 135,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 84, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 136,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 39, 28 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 137,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 62, 28 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 138,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 62, 28 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 139,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 43, 29 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 140,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 109, 29 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 141,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 53, 29 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 142,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 69, 29 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 143,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 68, 29 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 144,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 25, 29 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 145,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 107, 29 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 146,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 48, 29 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 147,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 36, 29 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 148,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 29, 30 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 149,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 21, 30 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 150,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 60, 30 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 151,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 58, 30 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 152,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 100, 30 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 153,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 55, 31 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 154,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 94, 31 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 155,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 88, 31 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 156,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 63, 31 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 157,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 60, 31 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 158,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 38, 31 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 159,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 21, 33 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 160,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 123, 33 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 161,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 27, 34 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 162,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 109, 34 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 163,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 46, 34 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 164,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 96, 34 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 165,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 114, 34 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 166,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 41, 34 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 167,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 64, 35 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 168,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 86, 35 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 169,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 45, 35 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 170,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 33, 35 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 171,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 110, 36 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 172,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 40, 36 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 173,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 80, 36 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 174,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 22, 36 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 175,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 108, 37 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 176,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 105, 37 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 177,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 119, 37 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 178,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 44, 37 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 179,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 120, 37 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 180,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 118, 37 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 181,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 122, 37 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 182,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 102, 37 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 183,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 35, 37 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 184,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 44, 37 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 185,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 58, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 186,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 77, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 187,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 61, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 188,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 21, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 189,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 24, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 190,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 101, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 191,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 45, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 192,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 59, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 193,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 24, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 194,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 122, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 195,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 32, 39 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 196,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 46, 39 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 197,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 114, 39 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 198,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 117, 39 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 199,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 62, 39 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 200,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 114, 39 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 201,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 108, 40 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 202,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 49, 41 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 203,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 75, 41 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 204,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 90, 42 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 205,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 48, 42 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 206,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 95, 42 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 207,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 66, 42 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 208,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 33, 42 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 209,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 44, 42 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 210,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 118, 43 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 211,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 44, 43 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 212,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 25, 43 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 213,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 85, 43 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 214,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 102, 43 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 215,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 30, 43 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 216,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 99, 44 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 217,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 27, 44 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 218,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 69, 44 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 219,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 23, 44 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 220,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 26, 44 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 221,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 45, 44 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 222,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 67, 44 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 223,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 74, 44 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 224,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 35, 45 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 225,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 122, 45 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 226,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 102, 45 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 227,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 86, 45 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 228,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 94, 45 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 229,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 67, 45 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 230,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 57, 45 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 231,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 64, 45 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 232,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 22, 46 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 233,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 104, 46 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 234,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 111, 46 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 235,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 94, 46 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 236,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 91, 46 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 237,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 22, 46 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 238,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 76, 46 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 239,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 92, 46 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 240,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 34, 46 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 241,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 43, 46 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 242,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 44, 47 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 243,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 69, 47 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 244,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 32, 47 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 245,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 67, 47 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 246,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 109, 47 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 247,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 74, 47 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 248,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 118, 47 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 249,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 90, 48 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 250,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 24, 48 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 251,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 84, 48 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 252,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 87, 48 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 253,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 51, 48 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 254,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 53, 48 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 255,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 80, 49 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 256,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 104, 49 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 257,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 116, 49 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 258,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 39, 49 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 259,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 65, 49 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 260,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 36, 49 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 261,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 43, 49 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 262,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 77, 49 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 263,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 67, 50 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 264,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 59, 50 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 265,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 87, 50 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 266,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 92, 50 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 267,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 39, 50 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 268,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 86, 50 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 269,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 45, 50 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 270,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 115, 50 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 271,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 68, 51 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 272,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 35, 51 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 273,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 105, 51 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 274,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 98, 51 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 275,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 75, 51 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 276,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 111, 51 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 277,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 56, 51 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 278,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 22, 51 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 279,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 87, 52 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 280,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 123, 52 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 281,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 63, 52 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 282,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 34, 52 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 283,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 36, 53 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 284,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 55, 53 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 285,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 22, 53 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 286,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 83, 55 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 287,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 91, 55 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 288,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 108, 55 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 289,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 29, 55 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 290,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 24, 55 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 291,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 52, 55 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 292,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 42, 55 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 293,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 109, 55 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 294,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 73, 55 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 295,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 97, 56 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 296,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 24, 56 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 297,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 85, 56 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 298,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 31, 56 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 299,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 27, 56 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 300,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 45, 56 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 301,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 98, 56 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 302,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 36, 57 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 303,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 31, 57 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 304,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 121, 57 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 305,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 62, 57 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 306,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 101, 57 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 307,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 48, 57 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 308,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 88, 59 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 309,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 55, 59 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 310,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 65, 60 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 311,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 35, 60 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 312,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 26, 60 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 313,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 90, 61 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 314,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 22, 61 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 315,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 24, 61 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 316,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 109, 61 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 317,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 79, 62 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 318,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 63, 62 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 319,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 31, 62 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 320,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 100, 62 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 321,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 65, 62 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 322,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 66, 63 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 323,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 29, 63 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 324,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 115, 63 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 325,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 84, 63 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 326,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 35, 63 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 327,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 58, 63 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 328,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 88, 64 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 329,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 71, 65 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 330,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 42, 65 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 331,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 89, 65 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 332,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 79, 65 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 333,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 115, 65 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 334,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 42, 65 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 335,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 34, 65 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 336,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 42, 65 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 337,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 109, 65 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 338,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 96, 65 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 339,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 58, 66 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 340,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 51, 66 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 341,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 30, 66 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 342,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 98, 66 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 343,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 22, 66 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 344,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 59, 66 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 345,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 40, 66 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 346,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 116, 66 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 347,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 30, 66 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 348,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 25, 67 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 349,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 33, 67 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 350,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 76, 67 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 351,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 119, 67 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 352,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 119, 67 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 353,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 97, 67 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 354,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 108, 67 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 355,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 94, 67 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 356,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 82, 67 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 357,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 44, 68 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 358,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 23, 68 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 359,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 27, 68 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 360,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 90, 68 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 361,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 54, 68 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 362,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 105, 69 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 363,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 116, 69 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 364,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 23, 69 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 365,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 55, 69 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 366,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 57, 69 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 367,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 62, 70 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 368,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 30, 70 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 369,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 83, 70 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 370,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 48, 70 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 371,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 36, 70 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 372,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 55, 70 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 373,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 67, 70 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 374,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 87, 70 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 375,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 44, 70 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 376,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 41, 71 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 377,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 85, 71 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 378,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 105, 71 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 379,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 118, 71 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 380,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 99, 72 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 381,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 90, 72 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 382,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 59, 72 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 383,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 114, 72 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 384,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 22, 72 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 385,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 39, 72 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 386,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 72, 74 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 387,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 113, 74 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 388,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 54, 74 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 389,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 25, 74 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 390,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 58, 74 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 391,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 49, 75 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 392,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 102, 75 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 393,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 49, 75 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 394,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 105, 75 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 395,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 68, 75 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 396,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 101, 75 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 397,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 97, 76 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 398,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 68, 76 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 399,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 105, 77 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 400,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 80, 77 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 401,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 105, 77 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 402,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 92, 77 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 403,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 81, 77 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 404,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 67, 78 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 405,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 75, 78 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 406,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 120, 78 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 407,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 68, 78 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 408,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 68, 78 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 409,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 91, 78 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 410,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 94, 79 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 411,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 30, 79 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 412,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 98, 79 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 413,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 77, 79 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 414,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 119, 79 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 415,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 38, 79 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 416,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 63, 80 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 417,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 117, 80 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 418,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 35, 80 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 419,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 121, 80 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 420,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 70, 80 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 421,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 43, 80 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 422,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 98, 80 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 423,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 68, 81 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 424,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 76, 81 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 425,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 47, 81 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 426,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 64, 81 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 427,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 72, 81 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 428,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 89, 81 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 429,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 93, 81 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 430,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 68, 83 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 431,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 68, 83 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 432,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 109, 83 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 433,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 48, 83 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 434,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 119, 83 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 435,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 111, 83 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 436,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 60, 83 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 437,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 38, 83 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 438,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 52, 83 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 439,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 111, 84 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 440,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 79, 85 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 441,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 90, 85 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 442,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 96, 85 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 443,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 25, 85 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 444,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 59, 85 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 445,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 106, 86 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 446,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 23, 86 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 447,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 47, 86 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 448,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 108, 86 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 449,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 121, 86 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 450,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 34, 86 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 451,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 32, 87 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 452,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 123, 87 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 453,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 90, 87 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 454,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 44, 87 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 455,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 43, 87 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 456,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 48, 87 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 457,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 110, 87 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 458,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 115, 87 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 459,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 107, 87 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 460,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 76, 88 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 461,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 103, 88 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 462,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 51, 88 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 463,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 98, 88 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 464,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 94, 88 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 465,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 107, 89 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 466,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 58, 89 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 467,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 110, 89 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 468,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 86, 89 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 469,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 75, 89 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 470,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 97, 89 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 471,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 109, 89 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 472,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 25, 89 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 473,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 109, 89 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 474,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 50, 89 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 475,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 53, 90 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 476,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 63, 90 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 477,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 62, 90 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 478,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 51, 90 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 479,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 48, 90 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 480,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 84, 90 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 481,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 81, 91 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 482,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 58, 91 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 483,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 26, 91 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 484,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 80, 91 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 485,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 30, 91 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 486,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 98, 91 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 487,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 24, 91 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 488,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 80, 91 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 489,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 44, 91 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 490,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 29, 92 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 491,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 95, 92 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 492,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 69, 92 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 493,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 53, 92 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 494,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 56, 92 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 495,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 121, 93 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 496,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 94, 93 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 497,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 68, 93 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 498,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 83, 94 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 499,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 68, 94 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 500,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 33, 94 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 501,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 63, 94 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 502,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 52, 94 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 503,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 23, 96 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 504,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 92, 96 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 505,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 34, 96 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 506,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 86, 96 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 507,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 31, 96 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 508,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 51, 96 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 509,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 55, 96 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 510,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 21, 96 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 511,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 34, 96 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 512,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 64, 96 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 513,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 79, 97 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 514,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 22, 97 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 515,
                columns: new[] { "UserId", "ProjectId" },
                values: new object[] { 116, 97 });

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_ProjectId",
                table: "Tasks",
                column: "ProjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Projects_ProjectId",
                table: "Tasks",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Projects_ProjectId",
                table: "Tasks");

            migrationBuilder.DropIndex(
                name: "IX_Tasks_ProjectId",
                table: "Tasks");

            migrationBuilder.RenameColumn(
                name: "ProjectId",
                table: "Tasks",
                newName: "PerformerId");

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 24, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 52, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 67, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 76, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 86, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 82, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 110, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 88, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 69, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 27, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 60, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 75, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 77, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 83, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 52, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 90, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 106, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 98, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 84, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 111, 5 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 116, 5 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 92, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 54, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 44, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 66, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 62, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 123, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 50, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 47, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 84, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 119, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 79, 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 35, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 40, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 123, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 63, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 92, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 94, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 63, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 101, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 90, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 78, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 47, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 99, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 120, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 81, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 62, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 66, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 103, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 81, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 64, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 118, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 23, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 73, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 73, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 33, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 34, 11 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 97, 11 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 64, 11 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 100, 11 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 81, 11 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 111, 11 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 82, 11 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 88, 11 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 75, 11 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 104, 12 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 97, 12 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 52, 12 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 103, 12 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 110, 13 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 65, 13 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 36, 13 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 101, 13 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 85, 13 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 88, 13 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 115, 13 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 78, 13 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 75, 14 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 83, 14 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 58, 14 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 76, 14 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 123, 15 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 120, 15 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 26, 15 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 75, 16 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 97, 16 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 52, 18 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 115, 18 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 63, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 42, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 37, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 114, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 117, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 63, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 118, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 60, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 21, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 123, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 82, 20 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 92, 20 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 97, 20 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 82, 20 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 109, 20 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 67, 20 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 34, 20 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 57, 20 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 87, 20 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 57, 21 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 26, 21 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 111, 21 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 80, 21 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 108, 21 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 62, 23 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 55, 24 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 28, 24 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 27, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 53, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 109, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 28, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 80, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 55, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 85, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 92, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 23, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 28, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 40, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 21, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 102, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 108, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 28, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 131,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 98, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 132,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 46, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 133,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 77, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 134,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 105, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 135,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 84, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 136,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 39, 28 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 137,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 62, 28 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 138,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 62, 28 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 139,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 43, 29 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 140,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 109, 29 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 141,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 53, 29 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 142,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 69, 29 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 143,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 68, 29 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 144,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 25, 29 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 145,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 107, 29 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 146,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 48, 29 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 147,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 36, 29 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 148,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 29, 30 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 149,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 21, 30 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 150,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 60, 30 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 151,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 58, 30 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 152,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 100, 30 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 153,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 55, 31 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 154,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 94, 31 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 155,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 88, 31 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 156,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 63, 31 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 157,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 60, 31 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 158,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 38, 31 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 159,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 21, 33 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 160,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 123, 33 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 161,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 27, 34 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 162,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 109, 34 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 163,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 46, 34 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 164,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 96, 34 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 165,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 114, 34 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 166,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 41, 34 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 167,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 64, 35 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 168,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 86, 35 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 169,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 45, 35 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 170,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 33, 35 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 171,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 110, 36 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 172,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 40, 36 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 173,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 80, 36 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 174,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 22, 36 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 175,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 108, 37 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 176,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 105, 37 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 177,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 119, 37 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 178,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 44, 37 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 179,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 120, 37 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 180,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 118, 37 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 181,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 122, 37 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 182,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 102, 37 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 183,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 35, 37 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 184,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 44, 37 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 185,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 58, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 186,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 77, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 187,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 61, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 188,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 21, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 189,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 24, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 190,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 101, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 191,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 45, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 192,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 59, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 193,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 24, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 194,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 122, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 195,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 32, 39 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 196,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 46, 39 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 197,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 114, 39 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 198,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 117, 39 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 199,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 62, 39 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 200,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 114, 39 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 201,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 108, 40 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 202,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 49, 41 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 203,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 75, 41 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 204,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 90, 42 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 205,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 48, 42 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 206,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 95, 42 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 207,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 66, 42 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 208,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 33, 42 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 209,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 44, 42 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 210,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 118, 43 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 211,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 44, 43 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 212,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 25, 43 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 213,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 85, 43 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 214,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 102, 43 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 215,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 30, 43 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 216,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 99, 44 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 217,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 27, 44 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 218,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 69, 44 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 219,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 23, 44 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 220,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 26, 44 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 221,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 45, 44 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 222,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 67, 44 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 223,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 74, 44 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 224,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 35, 45 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 225,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 122, 45 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 226,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 102, 45 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 227,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 86, 45 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 228,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 94, 45 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 229,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 67, 45 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 230,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 57, 45 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 231,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 64, 45 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 232,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 22, 46 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 233,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 104, 46 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 234,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 111, 46 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 235,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 94, 46 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 236,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 91, 46 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 237,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 22, 46 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 238,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 76, 46 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 239,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 92, 46 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 240,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 34, 46 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 241,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 43, 46 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 242,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 44, 47 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 243,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 69, 47 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 244,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 32, 47 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 245,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 67, 47 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 246,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 109, 47 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 247,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 74, 47 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 248,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 118, 47 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 249,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 90, 48 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 250,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 24, 48 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 251,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 84, 48 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 252,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 87, 48 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 253,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 51, 48 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 254,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 53, 48 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 255,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 80, 49 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 256,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 104, 49 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 257,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 116, 49 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 258,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 39, 49 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 259,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 65, 49 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 260,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 36, 49 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 261,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 43, 49 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 262,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 77, 49 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 263,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 67, 50 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 264,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 59, 50 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 265,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 87, 50 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 266,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 92, 50 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 267,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 39, 50 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 268,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 86, 50 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 269,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 45, 50 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 270,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 115, 50 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 271,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 68, 51 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 272,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 35, 51 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 273,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 105, 51 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 274,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 98, 51 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 275,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 75, 51 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 276,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 111, 51 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 277,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 56, 51 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 278,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 22, 51 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 279,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 87, 52 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 280,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 123, 52 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 281,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 63, 52 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 282,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 34, 52 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 283,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 36, 53 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 284,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 55, 53 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 285,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 22, 53 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 286,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 83, 55 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 287,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 91, 55 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 288,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 108, 55 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 289,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 29, 55 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 290,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 24, 55 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 291,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 52, 55 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 292,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 42, 55 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 293,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 109, 55 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 294,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 73, 55 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 295,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 97, 56 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 296,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 24, 56 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 297,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 85, 56 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 298,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 31, 56 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 299,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 27, 56 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 300,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 45, 56 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 301,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 98, 56 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 302,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 36, 57 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 303,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 31, 57 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 304,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 121, 57 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 305,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 62, 57 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 306,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 101, 57 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 307,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 48, 57 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 308,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 88, 59 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 309,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 55, 59 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 310,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 65, 60 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 311,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 35, 60 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 312,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 26, 60 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 313,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 90, 61 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 314,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 22, 61 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 315,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 24, 61 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 316,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 109, 61 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 317,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 79, 62 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 318,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 63, 62 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 319,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 31, 62 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 320,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 100, 62 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 321,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 65, 62 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 322,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 66, 63 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 323,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 29, 63 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 324,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 115, 63 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 325,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 84, 63 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 326,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 35, 63 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 327,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 58, 63 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 328,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 88, 64 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 329,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 71, 65 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 330,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 42, 65 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 331,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 89, 65 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 332,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 79, 65 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 333,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 115, 65 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 334,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 42, 65 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 335,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 34, 65 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 336,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 42, 65 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 337,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 109, 65 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 338,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 96, 65 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 339,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 58, 66 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 340,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 51, 66 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 341,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 30, 66 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 342,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 98, 66 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 343,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 22, 66 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 344,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 59, 66 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 345,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 40, 66 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 346,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 116, 66 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 347,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 30, 66 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 348,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 25, 67 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 349,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 33, 67 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 350,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 76, 67 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 351,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 119, 67 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 352,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 119, 67 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 353,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 97, 67 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 354,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 108, 67 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 355,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 94, 67 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 356,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 82, 67 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 357,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 44, 68 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 358,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 23, 68 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 359,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 27, 68 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 360,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 90, 68 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 361,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 54, 68 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 362,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 105, 69 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 363,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 116, 69 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 364,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 23, 69 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 365,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 55, 69 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 366,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 57, 69 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 367,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 62, 70 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 368,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 30, 70 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 369,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 83, 70 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 370,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 48, 70 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 371,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 36, 70 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 372,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 55, 70 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 373,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 67, 70 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 374,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 87, 70 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 375,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 44, 70 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 376,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 41, 71 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 377,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 85, 71 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 378,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 105, 71 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 379,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 118, 71 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 380,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 99, 72 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 381,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 90, 72 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 382,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 59, 72 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 383,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 114, 72 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 384,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 22, 72 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 385,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 39, 72 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 386,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 72, 74 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 387,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 113, 74 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 388,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 54, 74 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 389,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 25, 74 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 390,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 58, 74 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 391,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 49, 75 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 392,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 102, 75 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 393,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 49, 75 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 394,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 105, 75 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 395,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 68, 75 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 396,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 101, 75 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 397,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 97, 76 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 398,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 68, 76 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 399,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 105, 77 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 400,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 80, 77 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 401,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 105, 77 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 402,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 92, 77 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 403,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 81, 77 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 404,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 67, 78 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 405,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 75, 78 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 406,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 120, 78 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 407,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 68, 78 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 408,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 68, 78 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 409,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 91, 78 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 410,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 94, 79 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 411,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 30, 79 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 412,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 98, 79 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 413,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 77, 79 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 414,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 119, 79 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 415,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 38, 79 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 416,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 63, 80 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 417,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 117, 80 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 418,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 35, 80 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 419,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 121, 80 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 420,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 70, 80 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 421,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 43, 80 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 422,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 98, 80 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 423,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 68, 81 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 424,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 76, 81 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 425,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 47, 81 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 426,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 64, 81 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 427,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 72, 81 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 428,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 89, 81 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 429,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 93, 81 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 430,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 68, 83 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 431,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 68, 83 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 432,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 109, 83 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 433,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 48, 83 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 434,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 119, 83 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 435,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 111, 83 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 436,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 60, 83 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 437,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 38, 83 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 438,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 52, 83 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 439,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 111, 84 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 440,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 79, 85 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 441,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 90, 85 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 442,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 96, 85 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 443,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 25, 85 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 444,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 59, 85 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 445,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 106, 86 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 446,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 23, 86 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 447,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 47, 86 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 448,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 108, 86 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 449,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 121, 86 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 450,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 34, 86 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 451,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 32, 87 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 452,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 123, 87 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 453,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 90, 87 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 454,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 44, 87 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 455,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 43, 87 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 456,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 48, 87 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 457,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 110, 87 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 458,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 115, 87 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 459,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 107, 87 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 460,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 76, 88 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 461,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 103, 88 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 462,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 51, 88 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 463,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 98, 88 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 464,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 94, 88 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 465,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 107, 89 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 466,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 58, 89 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 467,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 110, 89 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 468,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 86, 89 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 469,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 75, 89 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 470,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 97, 89 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 471,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 109, 89 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 472,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 25, 89 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 473,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 109, 89 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 474,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 50, 89 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 475,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 53, 90 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 476,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 63, 90 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 477,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 62, 90 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 478,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 51, 90 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 479,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 48, 90 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 480,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 84, 90 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 481,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 81, 91 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 482,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 58, 91 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 483,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 26, 91 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 484,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 80, 91 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 485,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 30, 91 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 486,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 98, 91 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 487,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 24, 91 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 488,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 80, 91 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 489,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 44, 91 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 490,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 29, 92 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 491,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 95, 92 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 492,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 69, 92 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 493,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 53, 92 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 494,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 56, 92 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 495,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 121, 93 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 496,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 94, 93 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 497,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 68, 93 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 498,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 83, 94 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 499,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 68, 94 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 500,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 33, 94 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 501,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 63, 94 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 502,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 52, 94 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 503,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 23, 96 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 504,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 92, 96 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 505,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 34, 96 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 506,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 86, 96 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 507,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 31, 96 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 508,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 51, 96 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 509,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 55, 96 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 510,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 21, 96 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 511,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 34, 96 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 512,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 64, 96 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 513,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 79, 97 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 514,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 22, 97 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 515,
                columns: new[] { "PerformerId", "UserId" },
                values: new object[] { 116, 97 });

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_UserId",
                table: "Tasks",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Projects_UserId",
                table: "Tasks",
                column: "UserId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
