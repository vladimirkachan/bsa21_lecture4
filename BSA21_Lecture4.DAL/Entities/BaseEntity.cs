﻿using System;
using Newtonsoft.Json;

namespace BSA21_Lecture4.DAL.Entities
{
    public abstract class BaseEntity 
    {
        [JsonProperty("id")]
        public int Id { get; set; }
    }
}
