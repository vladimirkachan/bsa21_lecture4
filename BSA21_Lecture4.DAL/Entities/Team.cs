﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace BSA21_Lecture4.DAL.Entities
{
    public class Team : BaseEntity
    {
        [JsonProperty("name")]
        [MinLength(2)]
        [MaxLength(100)]
        public string Name {get; set;}

        [JsonProperty("createdAt")]
        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime CreatedAt {get; set;}
    }
}
