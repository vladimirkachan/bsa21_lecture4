﻿using System;
using System.Linq;
using BSA21_Lecture4.DAL;
using BSA21_Lecture4.DAL.Context;
using BSA21_Lecture4.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Task = System.Threading.Tasks.Task;

namespace BSA21_Lecture4.ConsoleClient
{
    class Program
    {
        static async Task  Main()
        {
            Client client = new();

            await client.DemoProjects();
            Console.WriteLine("\n----------------------------------\n");
            await client.DemoTasks();
            Console.WriteLine("\n----------------------------------\n");
            await client.DemoTeams();
            Console.WriteLine("\n----------------------------------\n");
            await client.DemoUsers();
            Console.WriteLine("\n----------------------------------\n");
            await client.DemoTaskCount();
            Console.WriteLine("\n----------------------------------\n");
            await client.DemoTeamsAndUsers();
            Console.WriteLine("\n----------------------------------\n");
            await client.DemoTasksByProject(3);
            Console.WriteLine("\n----------------------------------\n");
            await client.DemoProjectsWithTasks();
            Console.WriteLine("\n----------------------------------\n");

            // - 1 -
            await client.DemoTaskCount();
            Console.WriteLine("\n----------------------------------\n");

            // - 2 -
            await client.DemoTasksOnPerformer(22, 100);
            Console.WriteLine("\n----------------------------------\n");

            // - 3 -
            await client.DemoFinishedTask(100, 2021);
            Console.WriteLine("\n----------------------------------\n");

            // - 4 -
            await client.DemoTeamsWithUsers();
            Console.WriteLine("\n----------------------------------\n");

            // - 5 -
            await client.DemoSortedUsers();
            Console.WriteLine("\n----------------------------------\n");

            // - 6 -
            await client.DemoCustomUser(30);

            // - 7 -
            await client.DemoCustomProject(96);

            Console.WriteLine("Done");
            Console.ReadKey();
        }

        static async Task  InitialInsertDbFromBsa()
        {
            await using DataContext db = new();
            var teamEntities = await Requests.GetTeams();
            foreach (var te in teamEntities)
                await db.Database.ExecuteSqlRawAsync(@"SET IDENTITY_INSERT dbo.Teams ON
                INSERT INTO dbo.Teams
                (Id, Name, CreatedAt)
                VALUES({0}, {1}, {2})
                SET IDENTITY_INSERT dbo.Teams OFF", te.Id, te.Name, te.CreatedAt);
            var projectEntities = await Requests.GetProjects();
            foreach (var pe in projectEntities)
                await db.Database.ExecuteSqlRawAsync(@"SET IDENTITY_INSERT dbo.Projects ON
                INSERT INTO dbo.Projects
                (Id, AuthorId, TeamId, [Name], [Description], Deadline, CreatedAt)
                VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6})
                SET IDENTITY_INSERT dbo.Projects OFF", pe.Id, pe.AuthorId, pe.TeamId, pe.Name, pe.Description,
                                                     pe.Deadline, pe.CreatedAt);
            var taskEntities = await Requests.GetTasks();
            foreach (var te in taskEntities)
                await db.Database.ExecuteSqlRawAsync(@"SET IDENTITY_INSERT dbo.Tasks ON
                INSERT INTO dbo.Tasks
                (Id, ProjectId, PerformerId, [Name], [Description], [State], CreatedAt, FinishedAt)
                VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7})
                SET IDENTITY_INSERT dbo.Tasks OFF", te.Id, te.ProjectId, te.PerformerId, te.Name, te.Description,
                                                     te.State, te.CreatedAt, te.FinishedAt);
            var userEntities = await Requests.GetUsers();
            foreach (var ue in userEntities)
                await db.Database.ExecuteSqlRawAsync(@"SET IDENTITY_INSERT dbo.Users ON
                INSERT INTO dbo.Users
                (Id, TeamId, FirstName, LastName, Email, RegisteredAt, BirthDay)
                VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6})
                SET IDENTITY_INSERT dbo.Users OFF ", ue.Id, ue.TeamId, ue.FirstName, ue.LastName, ue.Email,
                                                     ue.RegisteredAt, ue.BirthDay);
                                 

            await db.SaveChangesAsync();
        }

    }
}
