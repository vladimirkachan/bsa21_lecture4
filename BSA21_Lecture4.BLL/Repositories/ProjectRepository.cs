﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using BSA21_Lecture4.DAL;
using BSA21_Lecture4.DAL.Context;
using BSA21_Lecture4.DAL.Entities;

namespace BSA21_Lecture4.BLL.Repositories
{
    public sealed class ProjectRepository : BaseRepository<Project>
    {
        public ProjectRepository(DataContext context) : base(context) {}
        public override int LastId => db.Projects.OrderBy(i => i.Id).LastOrDefault()?.Id ?? -1;
        public override IEnumerable<Project> Get(Expression<Func<Project, bool>> filter = null)
        {
            if (filter == null) return db.Projects.AsEnumerable();
            var f = filter.Compile();
            var query = from i in db.Projects.AsEnumerable()
                        where f(i)
                        select i;
            return query.AsEnumerable();
        }
        public override Project Get(int id)
        {
            return db.Projects.Find(id);
        }
        public override Project Create(Project entity)
        {
            var value = db.Projects.Add(entity).Entity;
            db.SaveChanges();
            return value;
        }
        public override bool Update(Project entity)
        {
            var item = Get(entity.Id);
            if (item == null) return false;
            db.Projects.Remove(item);
            db.Projects.Add(entity);
            db.SaveChanges();
            return true;
        }
        public override bool Delete(int id)
        {
            var item = Get(id);
            if (item == null) return false;
            db.Projects.Remove(item);
            db.SaveChanges();
            return true;
        }
    }
}
