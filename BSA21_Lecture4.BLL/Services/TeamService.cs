﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AutoMapper;
using BSA21_Lecture4.BLL.Interfaces;
using BSA21_Lecture4.Common.DTO;
using BSA21_Lecture4.DAL.Entities;

namespace BSA21_Lecture4.BLL.Services
{
    public class TeamService : IService<Team, TeamDTO>
    {
        readonly IRepository<Team> repository;
        readonly IMapper mapper;

        public TeamService(IRepository<Team> repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public IEnumerable<TeamDTO> Get(Expression<Func<Team, bool>> filter = null)
        {
            return mapper.Map<IEnumerable<TeamDTO>>(repository.Get(filter));
        }
        public TeamDTO Get(int id)
        {
            var entity = repository.Get(id);
            return entity == null ? null : mapper.Map<TeamDTO>(entity);
        }
        public TeamDTO Create(TeamDTO dto)
        {
            dto.CreatedAt = DateTime.Now;
            var entity = mapper.Map<Team>(dto);
            var team = repository.Create(entity);
            return mapper.Map<TeamDTO>(team);
        }
        public bool Update(TeamDTO dto)
        {
            return repository.Update(mapper.Map<Team>(dto));
        }
        public bool Delete(int id)
        {
            return repository.Delete(id);
        }
        public Team GetEntity(int id)
        {
            return repository.Get(id);
        }
    }
}
