﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AutoMapper;
using BSA21_Lecture4.BLL.Interfaces;
using BSA21_Lecture4.Common.DTO;
using BSA21_Lecture4.DAL.Entities;

namespace BSA21_Lecture4.BLL.Services
{
    public class UserService : IService<User, UserDTO>
    {
        readonly IRepository<User> repository;
        readonly IMapper mapper;

        public UserService(IRepository<User> repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }
        public IEnumerable<UserDTO> Get(Expression<Func<User, bool>> filter = null)
        {
            return mapper.Map<IEnumerable<UserDTO>>(repository.Get(filter));
        }
        public UserDTO Get(int id)
        {
            var entity = repository.Get(id);
            return entity == null ? null : mapper.Map<UserDTO>(entity);
        }
        public UserDTO Create(UserDTO dto)
        {
            dto.RegisteredAt = DateTime.Now;
            var entity = mapper.Map<User>(dto);
            var user = repository.Create(entity);
            return mapper.Map<UserDTO>(user);
        }
        public bool Update(UserDTO dto)
        {
            return repository.Update(mapper.Map<User>(dto));
        }
        public bool Delete(int id)
        {
            return repository.Delete(id);
        }
        public User GetEntity(int id)
        {
            return repository.Get(id);
        }
    }
}
