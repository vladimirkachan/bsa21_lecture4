﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AutoMapper;
using BSA21_Lecture4.BLL.Interfaces;
using BSA21_Lecture4.Common.DTO;
using Task = BSA21_Lecture4.DAL.Entities.Task;

namespace BSA21_Lecture4.BLL.Services
{
    public class TaskService : IService<Task, TaskDTO>
    {
        readonly IRepository<Task> repository;
        readonly IMapper mapper;

        public TaskService(IRepository<Task> repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public IEnumerable<TaskDTO> Get(Expression<Func<Task, bool>> filter = null)
        {
            return mapper.Map<IEnumerable<TaskDTO>>(repository.Get(filter));
        }
        public TaskDTO Get(int id)
        {
            var entity = repository.Get(id);
            return entity == null ? null : mapper.Map<TaskDTO>(entity);
        }
        public TaskDTO Create(TaskDTO dto)
        {
            var entity = mapper.Map<Task>(dto);
            var task = repository.Create(entity);
            return mapper.Map<TaskDTO>(task);
        }
        public bool Update(TaskDTO dto)
        {
            return repository.Update(mapper.Map<Task>(dto));
        }
        public bool Delete(int id)
        {
            return repository.Delete(id);
        }
        public Task GetEntity(int id)
        {
            return repository.Get(id);
        }
    }
}
