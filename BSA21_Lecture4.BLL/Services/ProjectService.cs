﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AutoMapper;
using BSA21_Lecture4.BLL.Interfaces;
using BSA21_Lecture4.Common.DTO;
using BSA21_Lecture4.DAL.Entities;

namespace BSA21_Lecture4.BLL.Services
{
    public class ProjectService : IService<Project, ProjectDTO>
    {
        readonly IRepository<Project> repository;
        readonly IMapper mapper;

        public ProjectService(IRepository<Project> repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public IEnumerable<ProjectDTO> Get(Expression<Func<Project, bool>> filter = null)
        {
            return mapper.Map<IEnumerable<ProjectDTO>>(repository.Get(filter));
        }
        public ProjectDTO Get(int id)
        {
            var entity = repository.Get(id);
            return entity == null ? null : mapper.Map<ProjectDTO>(entity);
        }
        public ProjectDTO Create(ProjectDTO dto)
        {
            var entity = mapper.Map<Project>(dto);
            var project = repository.Create(entity);
            return mapper.Map<ProjectDTO>(project);
        }
        public bool Update(ProjectDTO dto)
        {
            return repository.Update(mapper.Map<Project>(dto));
        }
        public bool Delete(int id)
        {
            return repository.Delete(id);
        }
        public Project GetEntity(int id)
        {
            return repository.Get(id);
        }
    }
}
