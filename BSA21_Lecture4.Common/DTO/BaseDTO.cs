﻿using System;

namespace BSA21_Lecture4.Common.DTO
{
    public abstract class BaseDTO
    {
        public virtual int Id { get; set; }
    }
}
