﻿using BSA21_Lecture4.Common.DTO;
using BSA21_Lecture4.DAL.Entities;
using Task = BSA21_Lecture4.DAL.Entities.Task;

namespace BSA21_Lecture4.WebAPI
{
    public class AutoMapperConfiguration
    {
        public AutoMapper.MapperConfiguration Get()
        {
            var mapperConfiguration = new AutoMapper.MapperConfiguration(c =>
            {
                c.CreateMap<Team, TeamDTO>();
                c.CreateMap<TeamDTO, Team>();

                c.CreateMap<User, UserDTO>();
                c.CreateMap<UserDTO, User>();

                c.CreateMap<Project, ProjectDTO>();
                c.CreateMap<ProjectDTO, Project>();

                c.CreateMap<Task, TaskDTO>();
                c.CreateMap<TaskDTO, Task>();
            });
            return mapperConfiguration;
        }

    }
}
