﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BSA21_Lecture4.BLL.Interfaces;
using BSA21_Lecture4.Common.DTO;
using BSA21_Lecture4.DAL.Entities;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Task = BSA21_Lecture4.DAL.Entities.Task;

namespace BSA21_Lecture4.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectsController : ControllerBase
    {
        readonly IService<Project, ProjectDTO> projectService;
        readonly IService<Task, TaskDTO> taskService;
        readonly IService<User, UserDTO> userService;
        readonly IValidator<ProjectCreateDTO> validator;

        public ProjectsController(IService<Project, ProjectDTO> projectService,
                                  IService<Task, TaskDTO> taskService,
                                  IService<User, UserDTO> userService,
                                  IValidator<ProjectCreateDTO> validator)
        {
            this.projectService = projectService;
            this.taskService = taskService;
            this.userService = userService;
            this.validator = validator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectDTO>>> Get()
        {
            return Ok(await System.Threading.Tasks.Task.Run(() => projectService.Get()));
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<ProjectDTO>> Get(int id)
        {
            var item = await System.Threading.Tasks.Task.Run(() => projectService.Get(id));
            return item == null ? NotFound($"There is no such project with Id = {id}") : Ok(item);
        }

        [HttpPost]
        public async Task<ActionResult<ProjectDTO>> Post([FromBody] ProjectCreateDTO dto)
        {
            var validationResult = await validator.ValidateAsync(dto);
            if (!validationResult.IsValid) return BadRequest(validationResult.Errors.First().ToString());
            if (string.IsNullOrWhiteSpace(dto.Name)) return BadRequest("The Name field cannot be empty");
            var value = await System.Threading.Tasks.Task.Run(() => projectService.Create(dto));
            return CreatedAtAction("POST", value);
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<UserDTO>> Put(int id, [FromBody] ProjectCreateDTO dto)
        {
            var validationResult = await validator.ValidateAsync(dto);
            if (!validationResult.IsValid) return BadRequest(validationResult.Errors.First().ToString());
            dto.Id = id;
            var updated = await System.Threading.Tasks.Task.Run(() => projectService.Update(dto));
            return updated ? Ok(await System.Threading.Tasks.Task.Run(() => projectService.Get(id))) : NotFound($"No user with ID {id}");
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            var deleted = await System.Threading.Tasks.Task.Run(() => projectService.Delete(id));
            return deleted ? NoContent() : NotFound($"No project with ID {id}");
        }

        [HttpGet("{projectId:int}/taskCount")]
        public async Task<ActionResult<int>> TaskCount(int projectId)
        {
            return await System.Threading.Tasks.Task.Run((from t in taskService.Get(e => e.ProjectId == projectId) select t).Count);
        }

        [HttpGet("withTasks")]
        public async Task<IEnumerable<KeyValuePair<ProjectDTO, IEnumerable<TaskDTO>>>> GetProjectsWithTasks()
        {
            var query = from p in projectService.Get()
                    select KeyValuePair.Create(p, 
                                               from t in taskService.Get(te => te.ProjectId == p.Id)
                                               select t);
            return await System.Threading.Tasks.Task.Run(query.AsEnumerable);
        }

        [HttpGet("custom/{projectId:int}")]
        public async Task<ActionResult<CustomProjectDTO>> GetCustomProject(int projectId)
        {
            var customProject = await BuildCustomProject(projectId);
            return Ok(customProject);
        }
        async Task<CustomProjectDTO> BuildCustomProject(int projectId)
        {
            var project = projectService.Get(projectId);
            var tasks = taskService.Get(te => te.ProjectId == projectId);
            var query1 = from t in tasks orderby t.Description.Length select t;
            var query2 = from t in tasks orderby t.Name.Length select t;
            int? teamId = projectService.GetEntity(projectId).TeamId;
            var query3 = from u in userService.Get(ue => ue.TeamId == teamId) select u;
            var customProject = new CustomProjectDTO
            {
                Project = project,
                LongestDescriptionTask = await System.Threading.Tasks.Task.Run(query1.LastOrDefault),
                ShortestNameTask = await System.Threading.Tasks.Task.Run(query2.FirstOrDefault),
                TotalCountOfTeamUsers = await System.Threading.Tasks.Task.Run(query3.Count)
            };
            return customProject;
        }

    }
}
