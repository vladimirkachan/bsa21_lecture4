﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BSA21_Lecture4.BLL.Interfaces;
using BSA21_Lecture4.Common.DTO;
using BSA21_Lecture4.DAL.Entities;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Task = BSA21_Lecture4.DAL.Entities.Task;

namespace BSA21_Lecture4.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        readonly IService<User, UserDTO> userService;
        readonly IService<Task, TaskDTO> taskService;
        readonly IService<Project, ProjectDTO> projectService;
        readonly IValidator<UserCreateDTO> validator;

        public UsersController(IService<User, UserDTO> userService, 
                               IService<Task,TaskDTO> taskService,
                               IService<Project, ProjectDTO> projectService,
                               IValidator<UserCreateDTO> validator)
        {
            this.userService = userService;
            this.taskService = taskService;
            this.projectService = projectService;
            this.validator = validator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDTO>>> Get()
        {
            return Ok(await System.Threading.Tasks.Task.Run(() => userService.Get()));
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<UserDTO>> Get(int id)
        {
            var item = await System.Threading.Tasks.Task.Run(() => userService.Get(id));
            return item == null ? NotFound($"There is no such user with Id = {id}") : Ok(item);
        }

        [HttpPost]
        public async Task<ActionResult<UserDTO>> Post([FromBody] UserCreateDTO dto)
        {
            var validationResult = await validator.ValidateAsync(dto);
            if (!validationResult.IsValid) return BadRequest(validationResult.Errors.First().ToString());
            var value = await System.Threading.Tasks.Task.Run(() => userService.Create(dto));
            return CreatedAtAction("POST", value);
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<UserDTO>> Put(int id, [FromBody] UserCreateDTO dto)
        {
            var validationResult = await validator.ValidateAsync(dto);
            if (!validationResult.IsValid) return BadRequest(validationResult.Errors.First().ToString());
            dto.Id = id;
            var updated = await System.Threading.Tasks.Task.Run(() => userService.Update(dto));
            return updated ? Ok(await System.Threading.Tasks.Task.Run(() => userService.Get(id))) : NotFound($"No user with ID {id}");
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            var deleted = await System.Threading.Tasks.Task.Run(() => userService.Delete(id));
            return deleted ? NoContent() : NotFound($"No user with ID {id}");
        }

        [HttpGet("sortedByNameAndTaskNameLength")]
        public async Task<ActionResult<IEnumerable<KeyValuePair<UserDTO, IEnumerable<TaskDTO>>>>> GetSortedUsers()
        {
            var query = from user in userService.Get()
                        orderby user.FirstName, user.LastName
                        select KeyValuePair.Create(user, 
                                                   from task in taskService.Get(t => t.PerformerId == user.Id)
                                                   orderby task.Name.Length, task.Name.ToUpper()
                                                   select task);
            return Ok(await System.Threading.Tasks.Task.Run(query.AsEnumerable));
        }

        [HttpGet("custom/{userId:int}")]
        public async Task<ActionResult<CustomUserDTO>> GetUserObject(int userId)
        {
            var user = userService.Get(userId);
            int? teamId = userService.GetEntity(userId).TeamId;
            CustomUserDTO customUser;
            if (teamId == null) customUser = new CustomUserDTO {User = user};
            else
            {
                var projectsQuery = from p in projectService.Get(pe => pe.TeamId == teamId) orderby p.CreatedAt
                                    select p;
                var lastProject = await System.Threading.Tasks.Task.Run(projectsQuery.LastOrDefault);
                var totalTaskQuery = from t in taskService.Get(te => te.ProjectId == lastProject.Id) select t;
                int totalTaskUnderLastProject = await System.Threading.Tasks.Task.Run(totalTaskQuery.Count);
                var unperformedTaskQuery = from t in taskService.Get(te => te.PerformerId == userId &&
                                                                     (te.FinishedAt == null ||
                                                                     te.FinishedAt > DateTime.Now)) select t;
                var totalUnperformedTask = await System.Threading.Tasks.Task.Run(unperformedTaskQuery.Count);
                var longestTaskQuery = from t in taskService.Get(te => te.PerformerId == userId)
                                       orderby (t.FinishedAt ?? DateTime.Now) - t.CreatedAt select t;
                var longestTask = await System.Threading.Tasks.Task.Run(longestTaskQuery.LastOrDefault);
                customUser = new CustomUserDTO
                {
                    User = user,
                    LastProject = lastProject,
                    TotalTaskUnderLastProject = totalTaskUnderLastProject,
                    TotalUnperformedTask = totalUnperformedTask,
                    LongestTask = longestTask
                };
            }
            return Ok(customUser);
        }

    }
}
