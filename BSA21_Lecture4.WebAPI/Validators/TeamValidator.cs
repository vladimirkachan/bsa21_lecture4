﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BSA21_Lecture4.Common.DTO;
using FluentValidation;

namespace BSA21_Lecture4.WebAPI.Validators
{
    public class TeamValidator : AbstractValidator<TeamCreateDTO> 
    {
        public TeamValidator()
        {
            RuleFor(i => i.Name).NotEmpty().Length(2, 100).WithMessage("Team name length must be 2-100");
        }
    }
}
